# Akvelon test task 

## Task Description 
```
You need to implement task storage by the project. “Task” is an instance that contains at least 3 fields listed below:
1.     Id
2.     Task name
3.     Task description

The solution should provide an ability to easily add new fields to the Task entity.

Each task should be a part of only one project. Project is an entity that contains name, id (and also keeps Tasks entities). The program must be a Web API.

Functional requirements:
Ability to create / view / edit / delete information about projects
Ability to create / view / edit / delete task information
Ability to add and remove tasks from a project (one project can contain several tasks)
Ability to view all tasks in the project
WIll be a plus to have an ability to filter and sort projects with various methods (start at, end at, range, exact value, etc.) and by various fields (start date, priority, etc.)

Project information that should be stored:
the name of the project
project start date
project completion date
the current status of the project (enum: NotStarted, Active, Completed)
priority (int)

Task information that should be stored:
task name
task status (enum: ToDo / InProgress / Done)
description
priority (int)
```
## Technology stack
* Java 11
* Spring Boot
* Maven
* Rest API
* Spring Data JPA
* PostgreSQL
* Swagger
* Spring Test

## How to work
1 You have to create local PostgreSQL Database and put spring datasource properties (url, username, password) to environment variables in configuration

2 Run application

3 Run `packing.sql` to full the table

4 Open Swagger in web browser to read description of methods

5 Use `request.http` to check endpoints

