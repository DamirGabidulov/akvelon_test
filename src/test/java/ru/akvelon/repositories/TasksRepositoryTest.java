package ru.akvelon.repositories;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.jdbc.Sql;
import ru.akvelon.models.Task;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureEmbeddedDatabase(type = AutoConfigureEmbeddedDatabase.DatabaseType.POSTGRES, beanName = "dataSource",
        provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.ZONKY,
        refresh = AutoConfigureEmbeddedDatabase.RefreshMode.BEFORE_EACH_TEST_METHOD)
@Sql(scripts = {"classpath:schema.sql", "classpath:data.sql"})
public class TasksRepositoryTest {

    @Autowired
    private TasksRepository tasksRepository;

    @Test
    public void return_correct_count_of_tasks(){
        assertThat(tasksRepository.count(), is(2L));
    }

    @Test
    public void return_correct_list_of_tasks(){
        Page<Task> allByIsDeletedIsNull = tasksRepository.findAllByIsDeletedIsNull(Pageable.ofSize(3));
        assertThat(allByIsDeletedIsNull).extracting(task -> task.getName() + " - " + task.getProject().getName()).contains("test task 1 - java", "test task 2 - java");
    }
}
