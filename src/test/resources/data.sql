insert into project(project_id, name, start_date, completion_date, priority, status) values (1, 'java', '2022-01-01 13:24:26.000000', '2022-01-20 13:24:26.000000', 1, 'COMPLETED');
insert into task(task_id, name, description, priority, status, project_id) values (1, 'test task 1', 'run pc', 1, 'TO_DO', 1);
insert into task(task_id, name, description, priority, status, project_id) values (2, 'test task 2', 'open book', 2, 'DONE', 1);
insert into task(task_id, name, description, priority, status, project_id, is_deleted) values (3, 'test task 3', 'close book', 3, 'DONE', 1, true);
