drop table task;
drop table project;
create table project(
project_id serial primary key,
completion_date timestamp,
name varchar(255),
priority integer,
start_date timestamp,
status varchar(255),
is_deleted boolean
);
create table task (
    task_id serial primary key,
    description varchar(255),
    name varchar(255),
    priority integer,
    status varchar(255),
    is_deleted boolean,
    project_id bigint references project (project_id)
);