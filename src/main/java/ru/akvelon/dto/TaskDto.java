package ru.akvelon.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.akvelon.models.Task;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskDto {
    private Long id;
    private String name;
    private String description;
    private Integer priority;
    private Task.Status status;
    private Long projectId;
    private Boolean isDeleted;

    public static TaskDto from(Task task){
        return TaskDto.builder()
                .id(task.getId())
                .name(task.getName())
                .description(task.getDescription())
                .priority(task.getPriority())
                .status(task.getStatus())
                .projectId(task.getProject().getId())
                .isDeleted(task.getIsDeleted())
                .build();
    }

    public static List<TaskDto> from(List<Task> tasks){
        return tasks.stream().map(TaskDto::from).collect(Collectors.toList());
    }
}
