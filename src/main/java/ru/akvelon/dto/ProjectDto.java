package ru.akvelon.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.akvelon.models.Project;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectDto {
    private Long id;
    private String name;
    private Instant startDate;
    private Instant completionDate;
    private Project.Status status;
    private Integer priority;

    public static ProjectDto from(Project project){
        return ProjectDto.builder()
                .id(project.getId())
                .name(project.getName())
                .startDate(project.getStartDate())
                .completionDate(project.getCompletionDate())
                .status(project.getStatus())
                .priority(project.getPriority())
                .build();
    }

    public static List<ProjectDto> from(List<Project> projects){
        return projects.stream().map(ProjectDto::from).collect(Collectors.toList());
    }
}
