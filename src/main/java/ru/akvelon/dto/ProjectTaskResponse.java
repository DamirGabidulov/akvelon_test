package ru.akvelon.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectTaskResponse {
    private ProjectDto projectDto;
    private List<TaskDto> tasks;
}
