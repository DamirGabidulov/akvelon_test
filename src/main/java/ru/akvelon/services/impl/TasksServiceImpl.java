package ru.akvelon.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.akvelon.dto.TaskDto;
import ru.akvelon.models.Project;
import ru.akvelon.models.Task;
import ru.akvelon.repositories.ProjectsRepository;
import ru.akvelon.repositories.TasksRepository;
import ru.akvelon.services.TasksService;

import java.util.List;
import java.util.Optional;

import static ru.akvelon.dto.TaskDto.from;

@Service
@RequiredArgsConstructor
public class TasksServiceImpl implements TasksService {
    private final TasksRepository tasksRepository;
    private final ProjectsRepository projectsRepository;

    @Override
    public List<TaskDto> getTasks(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("id"));
        Page<Task> result = tasksRepository.findAllByIsDeletedIsNull(pageRequest);
        return from(result.getContent());
    }

    @Override
    public TaskDto getTaskById(Long taskId) {
        Task task = tasksRepository.findById(taskId).orElseThrow(() -> new IllegalArgumentException("Task with that id was not found"));
        return TaskDto.from(task);
    }

    @Override
    public TaskDto addTask(TaskDto taskDto) {
        Optional<Project> project = projectsRepository.findById(taskDto.getProjectId());
        Task task = Task.builder()
                .name(taskDto.getName())
                .description(taskDto.getDescription())
                .priority(taskDto.getPriority())
                .status(Task.Status.TO_DO)
                .build();
        project.ifPresent(task::setProject);
        return from(tasksRepository.save(task));
    }

    @Override
    public TaskDto updateTask(Long taskId, TaskDto taskDto) {
        Task task = tasksRepository.getById(taskId);
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setPriority(taskDto.getPriority());
        task.setStatus(taskDto.getStatus());
        tasksRepository.save(task);
        return TaskDto.from(task);
    }

    @Override
    public void deleteTask(Long taskId) {
        Task task = tasksRepository.findById(taskId).orElseThrow(() -> new IllegalArgumentException("Task with that id was not found"));
        task.setIsDeleted(true);
        tasksRepository.save(task);
    }
}
