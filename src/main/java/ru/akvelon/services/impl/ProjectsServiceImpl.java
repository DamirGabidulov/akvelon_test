package ru.akvelon.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.akvelon.dto.ProjectDto;
import ru.akvelon.dto.TaskDto;
import ru.akvelon.models.Project;
import ru.akvelon.models.Task;
import ru.akvelon.repositories.ProjectsRepository;
import ru.akvelon.repositories.TasksRepository;
import ru.akvelon.services.ProjectsService;
import ru.akvelon.services.TasksService;

import java.util.List;
import java.util.stream.Collectors;

import static ru.akvelon.dto.ProjectDto.from;
import static ru.akvelon.dto.TaskDto.from;

@Service
@RequiredArgsConstructor
public class ProjectsServiceImpl implements ProjectsService {
    private final ProjectsRepository projectsRepository;
    private final TasksRepository tasksRepository;
    private final TasksService tasksService;

    @Override
    public List<ProjectDto> getProjects(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("id"));
        Page<Project> result = projectsRepository.findAllByIsDeletedIsNull(pageRequest);
        return from(result.getContent());
    }

    @Override
    public ProjectDto getProjectById(Long projectId) {
        Project project = projectsRepository.findById(projectId).orElseThrow(() -> new IllegalArgumentException("Project with that id was not found"));
        return from(project);
    }

    @Override
    public ProjectDto addProject(ProjectDto projectDto) {
        Project newProject = Project.builder()
                .name(projectDto.getName())
                .startDate(projectDto.getStartDate())
                .completionDate(projectDto.getCompletionDate())
                .priority(projectDto.getPriority())
                .status(Project.Status.NOT_STARTED)
                .build();
        return from(projectsRepository.save(newProject));
    }

    @Override
    public ProjectDto updateProject(Long projectId, ProjectDto projectDto) {
        Project project = projectsRepository.findById(projectId).orElseThrow(() -> new IllegalArgumentException("Project with that id was not found"));
        project.setName(projectDto.getName());
        project.setStartDate(projectDto.getStartDate());
        project.setCompletionDate(projectDto.getCompletionDate());
        project.setStatus(projectDto.getStatus());
        project.setPriority(projectDto.getPriority());
        return from(projectsRepository.save(project));
    }

    /**
     * delete project with tasks
     * @param projectId - by this id all tasks in this project (and this project too) will be deleted
     */
    @Override
    public void deleteProject(Long projectId) {
        Project project = projectsRepository.findById(projectId).orElseThrow(() -> new IllegalArgumentException("Project with that id was not found"));
        project.setIsDeleted(true);
        List<Long> tasksId = project.getTasks().stream().map(Task::getId).collect(Collectors.toList());
        for (Long id : tasksId){
            tasksService.deleteTask(id);
        }
        projectsRepository.save(project);
    }

    @Override
    public List<TaskDto> getTasksByProjectId(Long projectId) {
        List<Task> result = tasksRepository.findAllByProjectIdAndIsDeletedIsNull(projectId);
        return from(result);
    }

    @Override
    public void deleteTaskFromProject(Long projectId, Long taskId) {
        Task task = tasksRepository.findById(taskId).orElseThrow(() -> new IllegalArgumentException("Task with that id was not found"));
        if (task.getProject().getId().equals(projectId)){
            task.setProject(null);
            tasksRepository.save(task);
        }
    }

    @Override
    public List<TaskDto> addTaskToProject(Long projectId, Long taskId) {
        Project project = projectsRepository.findById(projectId).orElseThrow(() -> new IllegalArgumentException("Project with that id was not found"));
        Task task = tasksRepository.findById(taskId).orElseThrow(() -> new IllegalArgumentException("Task with that id was not found"));
        task.setProject(project);
        tasksRepository.save(task);
        return getTasksByProjectId(projectId);
    }

}
