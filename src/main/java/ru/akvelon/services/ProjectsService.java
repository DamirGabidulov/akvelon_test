package ru.akvelon.services;

import ru.akvelon.dto.ProjectDto;
import ru.akvelon.dto.TaskDto;

import java.util.List;

public interface ProjectsService {
    List<ProjectDto> getProjects(int page, int size);

    ProjectDto getProjectById(Long projectId);

    ProjectDto addProject(ProjectDto projectDto);

    ProjectDto updateProject(Long projectId, ProjectDto projectDto);

    void deleteProject(Long projectId);

    List<TaskDto> getTasksByProjectId(Long projectId);

    void deleteTaskFromProject(Long projectId, Long taskId);

    List<TaskDto> addTaskToProject(Long projectId, Long taskId);
}
