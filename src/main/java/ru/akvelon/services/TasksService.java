package ru.akvelon.services;

import ru.akvelon.dto.TaskDto;

import java.util.List;

public interface TasksService {
    List<TaskDto> getTasks(int page, int size);

    TaskDto getTaskById(Long taskId);

    TaskDto addTask(TaskDto taskDto);

    TaskDto updateTask(Long taskId, TaskDto taskDto);

    void deleteTask(Long taskId);
}
