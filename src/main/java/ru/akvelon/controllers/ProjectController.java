package ru.akvelon.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.akvelon.dto.ProjectDto;
import ru.akvelon.dto.ProjectResponse;
import ru.akvelon.dto.ProjectTaskResponse;
import ru.akvelon.dto.TaskResponse;

@Tag(
        name = "Controller for work with projects",
        description = "Controller can show/add/update/delete project and add/delete task to project"
)
public interface ProjectController {

    @Operation(
            summary = "Get projects",
            description = "Allow to get projects with pagination"
    )
    ResponseEntity<ProjectResponse> getProjects(int page,int size);

    @Operation(
            summary = "Get project by id",
            description = "Allow to get project by id"
    )
    ResponseEntity<ProjectDto> getProjectById (Long projectId);

    @Operation(
            summary = "Get project tasks by project id",
            description = "Allow to get project tasks by project id"
    )
    ResponseEntity<TaskResponse> getTasksByProjectId (Long projectId);

    @Operation(
            summary = "Get full information about project",
            description = "Allow to get project tasks by project id and project information"
    )
    ResponseEntity<ProjectTaskResponse> getFulLProjectInfo (Long projectId);

    @Operation(
            summary = "Add project",
            description = "Allow to add project"
    )
    ProjectDto addProject(ProjectDto projectDto);

    @Operation(
            summary = "Update project",
            description = "Allow to update project fields by project id"
    )
    ProjectDto updateProject(Long projectId, ProjectDto projectDto);

    @Operation(
            summary = "Add task to project",
            description = "Allow to add task to project by project id and task id. This operation useful when task has been deleted"
    )
    ProjectTaskResponse addTaskToProject (Long projectId, Long taskId);

    @Operation(
            summary = "Delete project",
            description = "Allow to delete project with all tasks in this project, change flag IsDeleted to true"
    )
    void deleteProject(Long projectId);

    @Operation(
            summary = "Delete task from project",
            description = "Allow to delete task from project by project id and task id"
    )
    void deleteTaskFromProject(Long projectId, Long taskId);
}
