package ru.akvelon.controllers.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.akvelon.controllers.ProjectController;
import ru.akvelon.dto.ProjectDto;
import ru.akvelon.dto.ProjectResponse;
import ru.akvelon.dto.ProjectTaskResponse;
import ru.akvelon.dto.TaskResponse;
import ru.akvelon.services.ProjectsService;

@RestController
@RequestMapping("api/projects")
@RequiredArgsConstructor
public class ProjectControllerImpl implements ProjectController {
    private final ProjectsService projectsService;

    @GetMapping
    public ResponseEntity<ProjectResponse> getProjects(@RequestParam("page") int page,
                                                    @RequestParam("size") int size){
        return ResponseEntity.ok().body(ProjectResponse.builder().data(projectsService.getProjects(page, size)).build());
    }

    @GetMapping("{project-id}")
    public ResponseEntity<ProjectDto> getProjectById (@PathVariable("project-id") Long projectId){
        return ResponseEntity.ok().body(projectsService.getProjectById(projectId));
    }

    @GetMapping("{project-id}/tasks")
    public ResponseEntity<TaskResponse> getTasksByProjectId (@PathVariable("project-id") Long projectId){
        return ResponseEntity.ok().body(TaskResponse.builder().data(projectsService.getTasksByProjectId(projectId)).build());
    }

    @GetMapping("{project-id}/full-project-info")
    public ResponseEntity<ProjectTaskResponse> getFulLProjectInfo (@PathVariable("project-id") Long projectId){
        return ResponseEntity.ok().body(ProjectTaskResponse.builder()
                .projectDto(projectsService.getProjectById(projectId))
                .tasks(projectsService.getTasksByProjectId(projectId))
                .build());
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ProjectDto addProject(@RequestBody ProjectDto projectDto){
        return projectsService.addProject(projectDto);
    }

    @PutMapping("{project-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ProjectDto updateProject(@PathVariable("project-id") Long projectId, @RequestBody ProjectDto projectDto){
        return projectsService.updateProject(projectId, projectDto);
    }

    @PutMapping("{project-id}/{task-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ProjectTaskResponse addTaskToProject (@PathVariable("project-id") Long projectId, @PathVariable("task-id") Long taskId){
        return ProjectTaskResponse.builder()
                .projectDto(projectsService.getProjectById(projectId))
                .tasks(projectsService.addTaskToProject(projectId, taskId))
                .build();
    }

    @DeleteMapping("{project-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteProject(@PathVariable("project-id") Long projectId){
        projectsService.deleteProject(projectId);
    }

    @DeleteMapping("{project-id}/{task-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteTaskFromProject(@PathVariable("project-id") Long projectId, @PathVariable("task-id") Long taskId){
        projectsService.deleteTaskFromProject(projectId, taskId);
    }
}
