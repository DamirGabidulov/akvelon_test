package ru.akvelon.controllers.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.akvelon.controllers.TaskController;
import ru.akvelon.dto.TaskDto;
import ru.akvelon.dto.TaskResponse;
import ru.akvelon.services.TasksService;

@RestController
@RequestMapping("api/tasks")
@RequiredArgsConstructor
public class TaskControllerImpl implements TaskController {
    private final TasksService tasksService;

    @GetMapping
    public ResponseEntity<TaskResponse> getTasks(@RequestParam("page") int page,
                                            @RequestParam("size") int size){
        return ResponseEntity.ok().body(TaskResponse.builder().data(tasksService.getTasks(page, size)).build());
    }

    @GetMapping("/{task-id}")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable("task-id") Long taskId){
        return ResponseEntity.ok().body(tasksService.getTaskById(taskId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDto addTask(@RequestBody TaskDto taskDto){
        return tasksService.addTask(taskDto);
    }

    @PutMapping("/{task-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public TaskDto updateTask(@PathVariable("task-id") Long taskId, @RequestBody TaskDto taskDto){
        return tasksService.updateTask(taskId, taskDto);
    }

    @DeleteMapping("/{task-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteTask(@PathVariable("task-id") Long taskId){
        tasksService.deleteTask(taskId);
    }
}
