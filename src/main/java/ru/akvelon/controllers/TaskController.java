package ru.akvelon.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.akvelon.dto.TaskDto;
import ru.akvelon.dto.TaskResponse;

@Tag(
        name = "Controller for work with tasks",
        description = "Controller can show/add/update/delete task"
)
public interface TaskController {
    @Operation(
            summary = "Get tasks",
            description = "Allow to get tasks with pagination"
    )
    ResponseEntity<TaskResponse> getTasks(int page, int size);

    @Operation(
            summary = "Get task by id",
            description = "Allow to get task by id"
    )
    ResponseEntity<TaskDto> getTaskById(Long taskId);

    @Operation(
            summary = "Add task",
            description = "Allow to add task with status TODO, also can add a task to an existing project"
    )
    TaskDto addTask(TaskDto taskDto);

    @Operation(
            summary = "Update task",
            description = "Allow to update task fields by task id"
    )
    TaskDto updateTask(Long taskId, TaskDto taskDto);

    @Operation(
            summary = "Delete project",
            description = "Allow to delete task, change flag IsDeleted to true"
    )
    void deleteTask(Long taskId);
}
