package ru.akvelon.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.akvelon.models.Task;

import java.util.List;

public interface TasksRepository extends JpaRepository<Task, Long> {
    Page<Task> findAllByIsDeletedIsNull(Pageable pageable);
    List<Task> findAllByProjectIdAndIsDeletedIsNull(Long projectId);
}
