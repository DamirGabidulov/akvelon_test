package ru.akvelon.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.akvelon.models.Project;

public interface ProjectsRepository extends JpaRepository<Project, Long> {
    Page<Project> findAllByIsDeletedIsNull (Pageable pageable);
}
