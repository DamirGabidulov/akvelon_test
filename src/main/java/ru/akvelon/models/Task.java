package ru.akvelon.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Task {

    public enum Status{
        TO_DO, IN_PROGRESS, DONE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private Long id;

    private String name;

    private String description;

    private Integer priority;

    @Enumerated(value = EnumType.STRING)
    private Status status;

    @ManyToOne()
    @JoinColumn(name = "project_id")
    private Project project;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
