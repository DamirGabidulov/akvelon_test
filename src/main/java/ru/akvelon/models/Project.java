package ru.akvelon.models;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Project {

    public enum Status {
        NOT_STARTED, ACTIVE, COMPLETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    private Long id;

    private String name;

    @Column(name = "start_date", nullable = false)
    private Instant startDate;

    @Column(name = "completion_date", nullable = false)
    private Instant completionDate;

    @Enumerated(value = EnumType.STRING)
    private Status status;

    private Integer priority;

    @OneToMany(mappedBy = "project")
    @ToString.Exclude
    private List<Task> tasks;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
